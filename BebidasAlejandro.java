/*
 * BebidasAlejandro.java
 * 
 * Copyright 2020 Lenovo <Lenovo@DESKTOP-7I8V61A>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import javax.swing.JOptionPane;

public class BebidasAlejandro{
	
	private int gaseosas;
    private int batidosEnAgua;
    private int batidosEnLeche;
    
   ///
    
    public void getTotalGaseosas(){
        int total;
        
    }
    /// Metodo que ira aunmemtando la cuenta en caso de que seleccione una de las bebidas 
    public void Bebidas(){
        int op;
        op = Integer.parseInt(JOptionPane.showInputDialog(null,"BEBIDAS\n1- Gaseosas, 1000"
                    +"\n2- Batidos en agua, 1500\n3- Batidos en leche, 2000"));
        if(op == 1)
            gaseosas += 1000;
        if(op == 2) 
            batidosEnAgua += 1500;
        if(op == 3)
            batidosEnLeche += 2000;  
    }
    ///Constructor 
    public BebidasAlejandro() {
        gaseosas = 0;
        batidosEnAgua = 0;
        batidosEnLeche = 0;
    }
    
    
        ///Get & Set
    public int getGaseosas() {
        return gaseosas;
    }

    public void setGaseosas(int gaseosas) {
        this.gaseosas = gaseosas;
    }

    public int getBatidosEnAgua() {
        return batidosEnAgua;
    }

    public void setBatidosEnAgua(int batidosEnAgua) {
        this.batidosEnAgua = batidosEnAgua;
    }

    public int getBatidosEnLeche() {
        return batidosEnLeche;
    }

    public void setBatidosEnLeche(int batidosEnLeche) {
        this.batidosEnLeche = batidosEnLeche;
    }
}

