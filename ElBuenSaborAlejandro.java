/*
 * ElBuenSaborAlejandro.java
 * 
 * Copyright 2020 Lenovo <Lenovo@DESKTOP-7I8V61A>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

import javax.swing.JOptionPane;

public class ElBuenSaborAlejandro{
	
	public static void main (String[] args) {
		BebidasAlejandro B = new BebidasAlejandro();
        ComidaAlejandro C = new ComidaAlejandro();
        int opcion; ///Almacena la opcion que digita el usuario para el menu principal
        do{
            opcion = Integer.parseInt(JOptionPane.showInputDialog(null,"MENU PRINCIPAL\n1- Bebidas\n"
                    + "2- Comida Rapida\n3- Cuenta\n4- Salir"));
            switch(opcion){
                case 1:
                     B.Bebidas(); ///Metodo que nos muestra el menu de BEBIDAS
                    break;
                case 2:
                    C.Comidas(); ///Metodo que nos muestra el menu de COMIDAS
                    break;
                case 3: ///muestra la cuenta y calcula el descuento en caso de que el total sea mayor de 10000
                    double total = (B.getGaseosas()+B.getBatidosEnAgua()+B.getBatidosEnLeche()+
                                        C.getChalupas()+C.getPapiCarne()+C.getHamburguesaEspecial());
                    if(total > 10000)
                        total = total - (total*.10);
                    JOptionPane.showMessageDialog(null,"Gaseosas, "+B.getGaseosas()
                                +"\nBatidos en agua, "+B.getBatidosEnAgua()
                                +"\nBatidos en leche, "+B.getBatidosEnLeche()
                                +"\nHambuerguesa especial, "+C.getHamburguesaEspecial()
                                +"\nPapicarne, "+C.getPapiCarne()
                                +"\nChalupas, "+C.getChalupas()
                                +"\nTotal a pagar:   "+total); 
                    break;                  
                case 4: ///Sale del menu principal
                    JOptionPane.showMessageDialog(null,"Se perdera la cuenta.");
                break;
            }

        }while(opcion != 4);
        
	}
}

