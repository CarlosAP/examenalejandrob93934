/*
 * ComidaAlejandro.java
 * 
 * Copyright 2020 Lenovo <Lenovo@DESKTOP-7I8V61A>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import javax.swing.JOptionPane;

public class ComidaAlejandro {
	
	private int hamburguesaEspecial;
    private int papiCarne;
    private int chalupas;
    
    
     /// Metodo que ira aunmemtando la cuenta en caso de que seleccione una de las bebidas 
    public void Comidas(){
        int op;
        op = Integer.parseInt(JOptionPane.showInputDialog(null,"COMIDA RAPIDA\n1- Hamburguesa Especial, 2500"+"\n2- Papicarne, 1500\n3- Chalupas, 2000")); 
        if(op == 1)
            hamburguesaEspecial += 2500;
        if(op == 2) 
            papiCarne += 1500;
        if(op == 3)
            chalupas += 2000;   
    }
    
    
    ///Constructoar
    public ComidaAlejandro(){
        hamburguesaEspecial = 0;
        papiCarne = 0;
        chalupas = 0;
    }
    ///Get & Set
    public int getHamburguesaEspecial() {
        return hamburguesaEspecial;
    }

    public void setHamburguesaEspecial(int hamburguesaEspecial) {
        this.hamburguesaEspecial = hamburguesaEspecial;
    }

    public int getPapiCarne() {
        return papiCarne;
    }

    public void setPapiCarne(int papiCarne) {
        this.papiCarne = papiCarne;
    }

    public int getChalupas() {
        return chalupas;
    }

    public void setChalupas(int chalupas) {
        this.chalupas = chalupas;
    }
	
}

